
function getSfdcOpportunity(ajaxUrl){
	$.ajax({
		  url: ajaxUrl,
		  type: 'GET',
		  success: function(data) {
			  var content = "<table class='table table-hover'><thead style='font-weight: bold;'><tr><td>Name</td><td>Probability</td><td>Description</td><td>Stage Name</td><td>Created Date</td><td>Close Date</td><td>Last Modified Date</td></tr></thead><tbody>";
			  jQuery.each(data.records, function(i, record) {
			    content = content + "<tr><td>" + record.Name+ "</td><td>" + record.Probability+ "</td><td>" + record.Description+ "</td><td>" + record.StageName+ "</td><td>" + record.CreatedDate+ "</td><td>" + record.CloseDate+ "</td><td>" + record.LastModifiedDate+ "</td></tr>";
			  });
			  content += "</tbody></table>"
			  $('#sfdcOpportunity').html(content);
		  },
		  error: function(e) {
			  $('#sfdcOpportunity').html('Something went wrong...!!!');
		  }
	});
	$('#loader').hide();
}