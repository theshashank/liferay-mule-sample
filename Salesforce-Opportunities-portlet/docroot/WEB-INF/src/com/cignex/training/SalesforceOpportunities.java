package com.cignex.training;

import com.cignex.training.helper.SalesforceOpportunitiesHelper;
import com.liferay.util.bridges.mvc.MVCPortlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

/**
 * Portlet implementation class SalesforceOpportunities
 */
public class SalesforceOpportunities extends MVCPortlet {
 
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {

		String action = resourceRequest.getParameter("ACTION");

		if ("GET".equalsIgnoreCase(action)) {
			resourceResponse.setContentType("text/json");
			PrintWriter writer = resourceResponse.getWriter();
			try {
				writer.print(SalesforceOpportunitiesHelper.getAllOpportunity());
			} catch (Exception e) {
				e.printStackTrace();
				throw new PortletException("Exception while calling SFDC API..");
			}
			writer.close();
		} else {
			super.serveResource(resourceRequest, resourceResponse);
		}
	}

}
