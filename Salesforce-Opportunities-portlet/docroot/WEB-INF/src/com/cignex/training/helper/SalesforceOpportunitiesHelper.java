package com.cignex.training.helper;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.util.Map;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.map.ObjectMapper;

public class SalesforceOpportunitiesHelper {

	public static String CLIENT_ID = "3MVG9ZL0ppGP5UrDXatMZtrrspJfOScxMqE5va.AZQc3EYR24XCXvoeJyePNSFvOL43ev.pRMB8PmuACtYft0";
	public static String CLIENT_SECRET = "5057994107623793713";
	public static String USERNAME = "shashank.gupta@cignex.com";
	public static String SECURITY_TOKEN = "4mmP8fleCl06HywJa0UtYyMi";
	public static String PASSWORD = "Mobile@121";
	public static String LOGIN_URL = "https://login.salesforce.com/services/oauth2/token?grant_type=password";
	public static String INSTANCE_URL = "https://ap2.salesforce.com/services/data/v34.0/query";
	public static String QUERY = "SELECT CloseDate,CreatedById,CreatedDate,Description,Id,IsClosed,IsDeleted,IsPrivate,IsWon,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,Name,OwnerId,Probability,StageName,SystemModstamp,TotalOpportunityQuantity,Type FROM Opportunity";
	public static String ACCESS_TOKEN = "access_token";
	
	private static final Log  log  = LogFactoryUtil.getLog(SalesforceOpportunitiesHelper.class);
	
	public static void main(String [] args) throws Exception{
		System.out.println(getAllOpportunity());
	}
	
	public static String getAllOpportunity() throws Exception{
        RequestBuilder builder = RequestBuilder.get(INSTANCE_URL).addParameter("q", QUERY);
        builder.addHeader(new BasicHeader("Authorization", String.format("Bearer %s", fetchAccessToken())));
        CloseableHttpResponse response = HttpClients.createDefault().execute(builder.build());
        String body = EntityUtils.toString(response.getEntity());
        ObjectMapper objectMapper = new ObjectMapper();
        log.debug(body);
        return objectMapper.writeValueAsString(new ObjectMapper().readValue(body, Map.class));
	}
		
	@SuppressWarnings("unchecked")
	private static String fetchAccessToken() throws Exception {
        log.debug("About to fetch access token");
        String loginURL = LOGIN_URL + "&client_id=" + CLIENT_ID + "&client_secret=" + CLIENT_SECRET + "&username=" + USERNAME + "&password=" + PASSWORD + SECURITY_TOKEN;
        HttpPost httpPost = new HttpPost(loginURL);
        CloseableHttpResponse response = HttpClients.createDefault().execute(httpPost);
        String body = EntityUtils.toString(response.getEntity());
        log.debug(String.format("SalesForce Response:\nCode:%s\nBody:%s", response.getStatusLine().getStatusCode(), body));
		Map<String, String> responseKeys = new ObjectMapper().readValue(body, Map.class);
        if (!responseKeys.containsKey(ACCESS_TOKEN)) {
            throw new Exception(ToStringBuilder.reflectionToString(responseKeys));
        }
        String accessToken = responseKeys.get(ACCESS_TOKEN);
        log.debug(String.format("Successfully obtained the accessToken: %s. This is a secure value. Make sure this is not showing up in production log.",
                            accessToken));
        return accessToken;
    }
}

