<%@page import="com.liferay.portal.kernel.servlet.SessionErrors"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<portlet:defineObjects />

<portlet:resourceURL var="defaultWeather">
	<portlet:param name="ACTION" value="GET"/>
	<portlet:param name="TYPE" value="DEFAULT"/>
</portlet:resourceURL>

<portlet:resourceURL var="cityWeather">
	<portlet:param name="ACTION" value="GET"/>
	<portlet:param name="TYPE" value="CITY"/>
</portlet:resourceURL>

<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="<%= request.getContextPath()%>/js/main.js?v1.14"></script>

<div id="defaultWeather"><img src="<%= request.getContextPath()%>/images/loading.gif" /></div>
<hr />
Select City : <select id="cities" onchange="javascript:getCityWeather('cities','<%= request.getContextPath()%>', '${cityWeather}&<portlet:namespace />city=');">
<option value="">--Select City--</option>
<option value="Delhi">Delhi</option>
<option value="Mumbai">Mumbai</option>
<option value="Kolkata">Kolkata</option>
<option value="Chennai">Chennai</option>
<option value="Lucknow">Lucknow</option>
<option value="Hyderabad">Hyderabad</option>
<option value="Thiruvananthapuram">Thiruvananthapuram</option>
<option value="Bangalore">Bangalore</option>
<option value="Chandigarh">Chandigarh</option>
<option value="Bhopal">Bhopal</option>
<option value="Gandhinagar">Gandhinagar</option>
</select>
<hr/>
<div id="cityWeather">
	Please select city to view its temperature.
</div>

<script type="text/javascript">
$(function() {
	 getDefaultTemperature('${defaultWeather}');
});
</script>
