function kelvinToCelcius(k){
	return floorFigure(k - 273.15, 2);
}
function getDefaultTemperature(ajaxUrl){
	$.ajax({
		  url: ajaxUrl,
		  type: 'GET',
		  success: function(data) {
			  $('#defaultWeather').html('<img src="http://openweathermap.org/img/w/' + data.weather[0].icon + '.png" />' + data.name + ", " + data.sys.country + 
				"<br/><strong>Current Temperature : </strong>" + kelvinToCelcius(data.main.temp) + " &#x2103" +
				"<br/><strong>Condition : </strong>" + data.weather[0].main + 
				"<br/><strong>Cloudiness : </strong>" + data.weather[0].description +
				"<br/><strong>Pressure : </strong>" + data.main.pressure + " hpa" +
				"<br/><strong>Humidity : </strong>" + data.main.humidity + "%"
			  );
		  },
		  error: function(e) {
			  $('#defaultWeather').html('Something went wrong...!!!');
		  }
		});
}

function getCityWeather(selectId, contextPath, ajaxUrl){
	var selectedVal = $('#'+selectId).val();
	if("" == selectedVal){
		$('#cityWeather').html('Please select city to view its temperature.');
		return false;
	}else{
		$('#cityWeather').html('<img src="' + contextPath + '/images/loading.gif" />');
		$.ajax({
			  url: ajaxUrl + selectedVal,
			  type: 'GET',
			  success: function(data) {
				  $('#cityWeather').html('<img src="http://openweathermap.org/img/w/' + data.weather[0].icon + '.png" />' + data.name + ", " + data.sys.country + 
					"<br/><strong>Current Temperature : </strong>" + kelvinToCelcius(data.main.temp) + " &#x2103" + 
					"<br/><strong>Condition : </strong>" + data.weather[0].main + 
					"<br/><strong>Cloudiness : </strong>" + data.weather[0].description +
					"<br/><strong>Pressure : </strong>" + data.main.pressure + " hpa" +
					"<br/><strong>Humidity : </strong>" + data.main.humidity + "%"
				  );
			  },
			  error: function(e) {
				  $('#cityWeather').html('Something went wrong...!!!');
			  }
			});
	}
}

function getDefaultCurrency(ajaxUrl){
	$.ajax({
		  url: ajaxUrl,
		  type: 'GET',
		  success: function(data) {
			  $('#defaultCurrency').html("1 USD = " + data.ConversionRateResponse.ConversionRateResult + " INR");
		  },
		  error: function(e) {
			  $('#defaultCurrency').html('Something went wrong...!!!');
		  }
	});
}

function convertCurrency(currencyInput, from, to, contextPath, ajaxUrl, fromParam, toParam){
	var selectedFrom = $('#'+from).val();
	var selectedTo = $('#'+to).val();
	var currInput  = $('#currencyInput').val();
	console.log(selectedFrom);
	console.log(selectedTo);
	console.log(currInput);
	if("" == selectedFrom && "" == selectedTo && "" == currInput){
		$('#countryCurrency').html('Please select/enter currencies to view their conversion rate.');
		return false;
	}else{
		$('#countryCurrency').html('<img src="' + contextPath + '/images/loading.gif" />');
		$.ajax({
			  url: ajaxUrl + fromParam + selectedFrom + toParam + selectedTo,
			  type: 'GET',
			  success: function(data) {
				  $('#countryCurrency').html(currInput + " " + selectedFrom + " = " + floorFigure(data.ConversionRateResponse.ConversionRateResult*currInput, 2) + " " + selectedTo);
			  },
			  error: function(e) {
				  $('#countryCurrency').html('Something went wrong...!!!');
			  }
			});
	}
}

function getSfdcOpportunity(ajaxUrl){
	$('#sfdcOpportunity').jqGrid({
	   	url: ajaxUrl,
		datatype: "json",
	   	colNames:['Name','Probability', 'Description', 'Stage Name','Created Date','Close Date','Last Modified Date'],
	   	colModel:[
	   		{name:'Name',index:'name', width:200, align:"left"},
	   		{name:'Probability',index:'probability', width:60},
	   		{name:'Description',index:'description', width:160},
	   		{name:'StageName',index:'stageName', width:100},
	   		{name:'CreatedDate',index:'createdDate', width:150, align:"right"},		
	   		{name:'CloseDate',index:'closedDate', width:130,align:"right"},		
	   		{name:'LastModifiedDate',index:'modifiedDate', width:150, sortable:false}		
	   	],
	   	rowNum:10,
	   	rowList:[10,25,50,100],
	   	sortname: 'Name',
	   	pager: '#pager',
	   	height: "100%",
	   	loadOnce: true,
	    loadtext: 'Please wait...',
	    viewrecords: true,
	    sortorder: "desc",    
	    caption:"Salesforce Opportunities"
	}).navGrid('#pager',
		{
		add:false,
		edit:false,
		del:false,
		search:false,
		refresh:true
		});
	$('#loader').hide();
}

function floorFigure(figure, decimals){
    if (!decimals) decimals = 2;
    var d = Math.pow(10,decimals);
    return (parseInt(figure*d)/d).toFixed(decimals);
}