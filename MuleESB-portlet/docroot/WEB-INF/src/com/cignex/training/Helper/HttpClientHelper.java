package com.cignex.training.Helper;


import java.io.IOException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;

public class HttpClientHelper {

	public static String WEATHER_PARAM_ESB = "http://localhost:8085/MuleESB/rest/weather?city=";
	public static String WEATHER_SAMPLE_ESB = "http://localhost:8085/MuleESB/rest/weather?city=Noida,IN";
	
	public static String CURRENCY_PARAM_ESB = "http://localhost:8085/MuleESB/soap/conversionRate?from=";
	public static String CURRENCY_SAMPLE_ESB = "http://localhost:8085/MuleESB/soap/example";
	
	public static String SALESFORCE_OPPORTUNITY_ESB = "http://localhost:8086/MuleSalesforce/getAllOpportunity";
	
	public static byte[] getHTTPResource(String requestURL) throws HttpException, IOException{
		HttpClient client = new HttpClient();
		HttpMethod get = new GetMethod(requestURL);
		UsernamePasswordCredentials credentials = new UsernamePasswordCredentials("shashank", "shashank");
		client.getState().setCredentials(AuthScope.ANY, credentials);
		get.setDoAuthentication(Boolean.TRUE);
		client.executeMethod(get);
		byte[] responseBody = get.getResponseBody();
		get.releaseConnection();
		return responseBody;
	}
	
	public static void main(String [] arg) throws HttpException, IOException{
		System.out.println(new String(HttpClientHelper.getHTTPResource(HttpClientHelper.WEATHER_SAMPLE_ESB)));
	}
}
