package com.cignex.training;

import com.cignex.training.Helper.HttpClientHelper;
import com.liferay.util.bridges.mvc.MVCPortlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

/**
 * Portlet implementation class SFDCOpportunity
 */
public class SFDCOpportunity extends MVCPortlet {
 
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {

		String action = resourceRequest.getParameter("ACTION");

		if ("GET".equalsIgnoreCase(action)) {
			String requestUrl = HttpClientHelper.SALESFORCE_OPPORTUNITY_ESB;
			resourceResponse.setContentType("text/json");
			PrintWriter writer = resourceResponse.getWriter();
			writer.print(new String(HttpClientHelper.getHTTPResource(requestUrl)));
			writer.close();
		} else {
			super.serveResource(resourceRequest, resourceResponse);
		}
	}

}
