package com.cignex.training;

import com.cignex.training.Helper.HttpClientHelper;
import com.liferay.util.bridges.mvc.MVCPortlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

/**
 * Portlet implementation class CurrencyConversionPortlet
 */
public class CurrencyConversionPortlet extends MVCPortlet {
 
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {

		String action = resourceRequest.getParameter("ACTION");
		String type = resourceRequest.getParameter("TYPE");

		if ("GET".equalsIgnoreCase(action)) {
			String requestUrl = null;
			if ("DEFAULT".equalsIgnoreCase(type)) {
				requestUrl = HttpClientHelper.CURRENCY_SAMPLE_ESB;
			} else {
				requestUrl = HttpClientHelper.CURRENCY_PARAM_ESB + resourceRequest.getParameter("from") + "&to=" + resourceRequest.getParameter("to");
			}
			resourceResponse.setContentType("text/json");
			PrintWriter writer = resourceResponse.getWriter();
			writer.print(new String(HttpClientHelper.getHTTPResource(requestUrl)));
			writer.close();
		} else {
			super.serveResource(resourceRequest, resourceResponse);
		}
	}

}
